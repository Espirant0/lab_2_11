#include <iostream>
#include "somecode.hpp"

using namespace saw;

int main() {
    CodeGenerator* somecode = codeFactory(PYTHON);
    std::cout << somecode->generateCode() << std::endl;
    
    delete somecode;
	return 0;
}